FROM adoptopenjdk/openjdk11:alpine-jre
ADD target/demo-service.jar demo-service.jar
ENTRYPOINT ["java", "-jar", "demo-service.jar"]