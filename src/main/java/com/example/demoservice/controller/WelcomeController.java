package com.example.demoservice.controller;

import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(WelcomeController.class);

    @RequestMapping("/hello")
    public ResponseEntity<?> getGreetings(){

        LOGGER.info("Running program, returning hello world");
        return new ResponseEntity<>("Hello world!", HttpStatus.OK);
    }

}
