## Set-up

This project is a simple spring-boot application that returns a string from a single API endpoint.

### What are the Prerequisites?

1. Enable Kubernetes - https://docs.docker.com/desktop/kubernetes/#enable-kubernetes
2. Install Kubectl - https://kubernetes.io/docs/tasks/tools/install-kubectl-macos/#install-with-homebrew-on-macos
3. Install kubectx - https://formulae.brew.sh/formula/kubectx
4. How to switch context -
   `kubectx' * list all the contexts
   `kubectx docker-desktop'


#### Deploying and running the application

1. If you make any changes to the application, you must create a new jar file using
```
mvn clean package
```
2. Build the image using the Dockerfile and the following command
```
docker build -t demo-service .
```
3. Create deployment and service for the application using the following
```
kubectl apply -f k8s/k8s.yaml
```
> ALTERNATIVELY: Run in a container using  
> 
>docker run -d -p 8080:8080 demo-service
